class Rectangle:
  def __init__(self, width, height):
    self.width = width
    self.height = height

  def __repr__(self):
    return 'Rectangle(width='+str(self.width)+', height='+str(self.height)+')'   

  def set_width(self, width):
    self.width = width

  def set_height(self, heigth):
    self.height = heigth

  def get_area(self): 
    return self.width * self.height

  def get_perimeter(self): 
    return 2 * (self.width + self.height)

  def get_diagonal(self): 
    return  (self.width**2 + self.height**2)**0.5

  def get_picture(self):
    
    if self.width > 50:
      return "Too big for picture."

    if self.height > 50:
      return "Too big for picture."      

    strwidth = str()
    for x in range(self.width):
      strwidth = strwidth + '*' 
    strwidth = strwidth + '\n'  

    strshape = str()
    for x in range(self.height):
      strshape = strshape + strwidth 
    return strshape

  def get_amount_inside(self, shape):
    n = int(self.width/shape.width) 
    m = int(self.height/shape.height) 
    return n*m


class Square(Rectangle):
  def __init__(self, side):
        self.side = side    
        self.width = side
        self.height = side

  def __repr__(self):
    return 'Square(side='+str(self.side)+')'   

  def set_width(self, side):
    self.side = side
    self.width = side
    self.height = side

  def set_height(self, side):
    self.side = side
    self.width = side
    self.height = side 

  def set_side(self, side):
    self.side = side
    self.width = side
    self.height = side 
